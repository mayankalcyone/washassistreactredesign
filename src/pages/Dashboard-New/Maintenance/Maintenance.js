import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"
const Maintenance = (props) => {
return (
   <React.Fragment>
            <div className="page-content">
                <MetaTags>
                <title>Maintenance</title>
                </MetaTags>
                <div className="container-fluid">
                <Breadcrumbs title="Maintenance" breadcrumbItem="Maintenance" />
                </div>
            </div>
        </React.Fragment>
    );
};
export default Maintenance;