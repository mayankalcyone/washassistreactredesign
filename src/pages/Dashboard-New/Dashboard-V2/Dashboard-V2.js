import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"
const DashboardV2 = (props) => {
return (
   <React.Fragment>
            <div className="page-content">
                <MetaTags>
                <title>Dashboard V2</title>
                </MetaTags>
                <div className="container-fluid">
                <Breadcrumbs title="DashboardV2" breadcrumbItem="DashboardV2" />
                </div>
            </div>
        </React.Fragment>
    );
};
export default DashboardV2;