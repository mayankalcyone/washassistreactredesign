import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"
const Dashboard = (props) => {
return (
   <React.Fragment>
            <div className="page-content">
                <MetaTags>
                <title>Dashboard</title>
                </MetaTags>
                <div className="container-fluid">
                <Breadcrumbs title="Dashboard" breadcrumbItem="Dashboard" />
                </div>
            </div>
        </React.Fragment>
    );
};
export default Dashboard;