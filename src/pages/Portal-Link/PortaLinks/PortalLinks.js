import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const PortalLinks = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Portal Links</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="PortalLink" breadcrumbItem="Portal Links" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default PortalLinks;
