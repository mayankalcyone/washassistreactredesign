import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const PortalLinkConfigurations = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Portal Link Configurations</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="PortalLink" breadcrumbItem="Portal Link Configurations" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default PortalLinkConfigurations;
