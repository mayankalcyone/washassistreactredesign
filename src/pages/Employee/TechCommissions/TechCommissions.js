import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const TechCommissions = (props) => {
   
    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>TechCommissions</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Employee" breadcrumbItem="Tech Commissions" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default TechCommissions;
