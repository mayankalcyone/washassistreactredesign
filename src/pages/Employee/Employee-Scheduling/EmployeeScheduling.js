import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const EmployeeScheduling = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Employee Scheduling</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Employee" breadcrumbItem="Employee Scheduling" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default EmployeeScheduling;
