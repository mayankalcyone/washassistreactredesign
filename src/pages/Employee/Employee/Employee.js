import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const Employee = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Employee</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Employee" breadcrumbItem="Employee" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default Employee;
