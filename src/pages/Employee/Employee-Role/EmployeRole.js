import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const EmployeeRole = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Employee Role</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Employee" breadcrumbItem="Employee Role" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default EmployeeRole;
