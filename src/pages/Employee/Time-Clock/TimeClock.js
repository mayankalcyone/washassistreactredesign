import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const TimeClock = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Time Clock</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Employee" breadcrumbItem="Time Clock" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default TimeClock;
