import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const Adjustments =() => {
    return (
        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Adjustments</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Transactions" breadcrumbItem="Adjustments" />
                </div>
            </div>

        </React.Fragment>
  )
}

export default Adjustments