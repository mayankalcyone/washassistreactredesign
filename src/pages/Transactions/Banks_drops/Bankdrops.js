import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const Bankdrops=()=> {
    return (
        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Bankdrops</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Transactions" breadcrumbItem="Bankdrops" />
                </div>
            </div>

        </React.Fragment>
  )
}

export default Bankdrops