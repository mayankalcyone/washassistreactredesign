import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const TicketsDetails =()=> {
    return (
        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>TicketsDetails</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="TTransactions" breadcrumbItem="Tickets Details" />
                </div>
            </div>

        </React.Fragment>
  )
}

export default TicketsDetails