import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const CreditCardTransactions=()=> {
    return (
        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Credit Card Transactions</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Transactions" breadcrumbItem="Credit Card Transactions" />
                </div>
            </div>

        </React.Fragment>
  )
}

export default CreditCardTransactions