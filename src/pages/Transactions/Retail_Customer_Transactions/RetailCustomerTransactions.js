import React from 'react';
import { Fragment } from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const RetailCustomerTransactions=()=> {
    return (
        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Retail Customer Transactions</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Transactions" breadcrumbItem="Retail Customer Transactions" />
                </div>
            </div>

        </React.Fragment>
  )
}

export default RetailCustomerTransactions
