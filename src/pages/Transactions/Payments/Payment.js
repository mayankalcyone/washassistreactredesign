import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const Payment=()=> {
    return (
        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Payment</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Transactions" breadcrumbItem="Payment" />
                </div>
            </div>

        </React.Fragment>
  )
}

export default Payment