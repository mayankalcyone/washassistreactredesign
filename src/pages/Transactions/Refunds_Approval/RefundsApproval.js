import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const RefundsApproval=()=> {
  return (
      <React.Fragment>
          <div className="page-content">
              <MetaTags>
                  <title>Refunds Approval</title>
              </MetaTags>
              <div className="container-fluid">
                  <Breadcrumbs title="Transactions" breadcrumbItem="Refunds Approval" />
              </div>
          </div>

      </React.Fragment>
  )
}

export default RefundsApproval