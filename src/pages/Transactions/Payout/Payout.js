import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const Payout=()=> {
  return (
      <React.Fragment>
          <div className="page-content">
              <MetaTags>
                  <title>Payout</title>
              </MetaTags>
              <div className="container-fluid">
                  <Breadcrumbs title="Transactions" breadcrumbItem="Payout" />
              </div>
          </div>

      </React.Fragment>
  )
}

export default Payout