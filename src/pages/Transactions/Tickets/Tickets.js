import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const Tickets= ()=> {
    return (
        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Transactions</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Transactions" breadcrumbItem="Tickets" />
                </div>
            </div>
        </React.Fragment>
  )
}

export default Tickets
