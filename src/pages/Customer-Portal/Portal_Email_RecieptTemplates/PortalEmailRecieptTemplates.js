import React from 'react'
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";  

const PortalEmailRecieptTemplates=()=> {
  return (
    <React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Portal Email Reciept Templates</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Customer Portal" breadcrumbItem="Portal Email Reciept Templates" />
        </div>
    </div>
</React.Fragment>
  )
}

export default PortalEmailRecieptTemplates