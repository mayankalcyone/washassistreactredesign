import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";  

const PortalScreenOptions=()=> {
  return (
    <React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Portal Screen Options</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Customer Portal" breadcrumbItem="Portal Screen Options" />
        </div>
    </div>
</React.Fragment>
  )
}

export default PortalScreenOptions