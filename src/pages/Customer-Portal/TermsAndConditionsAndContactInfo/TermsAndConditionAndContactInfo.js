import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"; 

const TermsAndConditionAndContactInfo=()=> {
  return (
    <React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Terms And Condition & ContactInfo</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Customer Portal" breadcrumbItem="Terms And Condition And ContactInfo" />
        </div>
    </div>
</React.Fragment>
  )
}

export default TermsAndConditionAndContactInfo