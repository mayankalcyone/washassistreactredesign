import React from 'react'
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";
const PortalCustomization=()=> {
  return (
      <React.Fragment>
          <div className='page-content'>
              <MetaTags>
                  <title>Portal Customization</title>
              </MetaTags>
              <div className="container-fluid">
                  <Breadcrumbs title="Customer Portal" breadcrumbItem="Portal Customization" />
              </div>
          </div>
      </React.Fragment>
  )
}

export default PortalCustomization