import React from 'react'
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";  

const  PortalNevigationOrderChange=()=> {
  return (
<React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Portal Nevigation Order Changes</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Customer Portal" breadcrumbItem="Portal Nevigation Order Change" />
        </div>
    </div>
</React.Fragment>
  )
}

export default PortalNevigationOrderChange