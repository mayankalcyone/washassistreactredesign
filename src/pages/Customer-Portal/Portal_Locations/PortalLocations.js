import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";  

const  PortalLocations=()=> {
  return (
    <React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Portal Locations</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Customer Portal" breadcrumbItem="PortalLocations" />
        </div>
    </div>
</React.Fragment>
  )
}

export default PortalLocations