import React from 'react'
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";  

const PortalProducts=()=> {
  return (
    <React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Portal Products</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Customer Portal" breadcrumbItem="Portal Products" />
        </div>
    </div>
</React.Fragment>
  )
}

export default PortalProducts