import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";  

const PortalSetting=()=> {
  return (
    <React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Portal Setting</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Customer Portal" breadcrumbItem="Portal Setting" />
        </div>
    </div>
</React.Fragment>
  )
}

export default PortalSetting