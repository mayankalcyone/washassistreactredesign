import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const ScheduleReports=()=> {
  return (
    <React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Schedule Reports</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Reports" breadcrumbItem="Schedule Reports" />
        </div>
    </div>
</React.Fragment>
  )
}

export default ScheduleReports