import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const LocationAudit = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Location Audit</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Audit" breadcrumbItem="Location Audit" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default LocationAudit;
