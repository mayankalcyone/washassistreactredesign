import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const RefundAudit = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Refund Audit</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Audit" breadcrumbItem="Refund Audit" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default RefundAudit;
