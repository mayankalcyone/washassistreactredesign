import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const TimeClockAudit = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Time Clock Audit</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Audit" breadcrumbItem="Time Clock Audit" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default TimeClockAudit;
