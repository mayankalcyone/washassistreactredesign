import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const Audit = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Audit</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Audit" breadcrumbItem="Audit" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default Audit;
