import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"
const EquipmentCategory = (props) => {
    return (
        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Equipment Category</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Maintenance" breadcrumbItem="Equipment Category" />
                </div>
            </div>
        </React.Fragment>
    );
};
export default EquipmentCategory;