import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"
const Tasks = (props) => {
    return (
        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Tasks</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Maintenance" breadcrumbItem="Tasks" />
                </div>
            </div>
        </React.Fragment>
    );
};
export default Tasks;
