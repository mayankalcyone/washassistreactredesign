import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"
const ChecklistCategory = (props) => {
return (
   <React.Fragment>
            <div className="page-content">
                <MetaTags>
                <title>Checklist Category</title>
                </MetaTags>
                <div className="container-fluid">
                <Breadcrumbs title="Maintenance" breadcrumbItem="Checklist Category" />
                </div>
            </div>
        </React.Fragment>
    );
};
export default ChecklistCategory;