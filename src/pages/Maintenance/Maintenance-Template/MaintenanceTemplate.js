import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"
const MaintenanceTemplate = (props) => {
    return (
        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>MaintenanceTemplate</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Maintenance" breadcrumbItem="Maintenance Template" />
                </div>
            </div>
        </React.Fragment>
    );
};
export default MaintenanceTemplate;