import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"
const VendorsCompany = (props) => {
    return (
        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Vendors Company</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Maintenance" breadcrumbItem="Vendors Company" />
                </div>
            </div>
        </React.Fragment>
    );
};
export default VendorsCompany;