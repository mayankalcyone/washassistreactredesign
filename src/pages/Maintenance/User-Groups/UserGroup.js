import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"
const UserGroups = (props) => {
    return (
        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>User Group</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Maintenance" breadcrumbItem="User Group" />
                </div>
            </div>
        </React.Fragment>
    );
};
export default UserGroups;