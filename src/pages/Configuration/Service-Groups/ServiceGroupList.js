import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const ServiceGroupList =()=>{
	return(
		
		<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>Service Group </title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Configuration " breadcrumbItem="Service Group "/>
			</div>
			</div>
			</React.Fragment>

		)
}
export default ServiceGroupList;