import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const GreetersButtonList =()=>{
	return(
		
		<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>Greeters Buttons</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Configuration " breadcrumbItem="Greeters Button"/>
			</div>
			</div>
			</React.Fragment>

		)
}
export default GreetersButtonList;