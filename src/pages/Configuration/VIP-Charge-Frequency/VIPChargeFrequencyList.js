import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const VIPChargeFrequencyList =()=>{
	return(
		
		<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>VIP Charge Frequency List</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Configuration " breadcrumbItem="VIP Charge Frequency List"/>
			</div>
			</div>
			</React.Fragment>

		)
}
export default VIPChargeFrequencyList;