import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const CancellationReasonList =()=>{
	return(
		
		<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>Cancellation Reason</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Configuration" breadcrumbItem="Cancellation Reasons"/>
			</div>
			</div>
			</React.Fragment>

		)
}
export default CancellationReasonList;