import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const LoyaltyDiscountTypeList =()=>{
	return(
		
		<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>Loyalty Discount Type</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Configuration " breadcrumbItem="Loyalty Discount Type "/>
			</div>
			</div>
			</React.Fragment>

		)
}
export default LoyaltyDiscountTypeList;