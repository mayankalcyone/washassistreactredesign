import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const DepartmentList =()=>{
	return(
		
		<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>Departments</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Configuration" breadcrumbItem="Department list"/>
			</div>
			</div>
			</React.Fragment>

		)
}
export default DepartmentList;