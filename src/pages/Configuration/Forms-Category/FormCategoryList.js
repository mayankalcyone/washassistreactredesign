import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const FormCategoryList =()=>{
	return(
		
		<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>Form Category</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Configuration" breadcrumbItem="Form Category"/>
			</div>
			</div>
			</React.Fragment>

		)
}
export default FormCategoryList;