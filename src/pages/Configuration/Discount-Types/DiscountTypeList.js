import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const DiscountTypeList =()=>{
	return(
		
		<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>Discounts Type</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Configuration" breadcrumbItem="Discounts Type"/>
			</div>
			</div>
			</React.Fragment>

		)
}
export default DiscountTypeList;