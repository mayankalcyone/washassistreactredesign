import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const VIPAccountLoyaltyDiscountsList =()=>{
	return(
		
		<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>VIP Account Loyalty discounts</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Configuration " breadcrumbItem="VIP Account Loyalty discounts"/>
			</div>
			</div>
			</React.Fragment>

		)
}
export default VIPAccountLoyaltyDiscountsList;