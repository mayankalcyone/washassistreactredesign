import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const IssueStatusTypeList =()=>{
	return(
		
		<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>issue Status Type </title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Configuration " breadcrumbItem="Issue Status Type"/>
			</div>
			</div>
			</React.Fragment>

		)
}
export default IssueStatusTypeList;