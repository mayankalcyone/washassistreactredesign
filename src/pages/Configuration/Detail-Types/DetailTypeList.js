import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const DetailTypeList =()=>{
	return(
		
		<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>Detail Types</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Configuration" breadcrumbItem="Detail Types"/>
			</div>
			</div>
			</React.Fragment>

		)
}
export default DetailTypeList;