import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const GeneralLedgersAccountCodeList =()=>{
	return(
		
		<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>General Ledgers Account codes</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Configuration " breadcrumbItem="General Ledgers account codes"/>
			</div>
			</div>
			</React.Fragment>

		)
}
export default GeneralLedgersAccountCodeList;