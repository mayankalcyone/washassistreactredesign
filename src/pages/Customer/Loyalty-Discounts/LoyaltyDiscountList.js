import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const LoyaltyDiscountList =()=>{
	return(
		
		<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>Loyalty Discount</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Customers " breadcrumbItem="Loyalty discount"/>
			</div>
			</div>
			</React.Fragment>

		)
}
export default LoyaltyDiscountList;