import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const RewardsAndUnlimitedProgramList =()=>{
return(
	<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>Reward And Unlimited programs</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Customers " breadcrumbItem="Reward And Unlimited Programs"/>
			</div>
			</div>
			</React.Fragment>	
)}
export default RewardsAndUnlimitedProgramList;