import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const GiftCardList =()=>{
	return(
			<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>Gift Card</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Customers" breadcrumbItem="Gift Card"/>
			</div>
			</div>
			</React.Fragment>	

		)
}
export default GiftCardList;