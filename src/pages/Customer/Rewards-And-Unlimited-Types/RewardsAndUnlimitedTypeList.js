import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const RewardsAndUnlimitedTypeList =()=>{

		return(
			<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>Reward And Unlimited Types</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Customers " breadcrumbItem="Reward And Unlimited Types"/>
			</div>
			</div>
			</React.Fragment>	

		)}
export default RewardsAndUnlimitedTypeList;