import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const PrepaidBookList =()=>{
return(

			<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>Prepaid Book</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Customers " breadcrumbItem="Prepaid Book"/>
			</div>
			</div>
			</React.Fragment>	


)}

export default PrepaidBookList;