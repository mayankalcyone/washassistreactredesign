import React from "react";
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const BundleList =(props)=>{
	
		return(
			<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>Bundle</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Customers" breadcrumbItem="Bundle" />
			</div>
			</div>
			</React.Fragment>
						)
		
}
export default BundleList;