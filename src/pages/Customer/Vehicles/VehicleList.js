import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const VehicleList =()=>{
	return(
		<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>Vehicle</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Customers" breadcrumbItem="Vehicle"/>
			</div>
			</div>
			</React.Fragment>
	)
}
export default VehicleList;