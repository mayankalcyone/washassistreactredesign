import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const RetailCustomerList =()=>{
	return(
		<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>Retail Customer</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Customers " breadcrumbItem="Retail Customer"/>
			</div>
			</div>
			</React.Fragment>	
	)
}
export default RetailCustomerList;