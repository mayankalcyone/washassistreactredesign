import React from "react";
import MetaTags from 'react-meta-tags';

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const HouseAccountList =()=>{
	return(
			<React.Fragment>
			<div className="page-content">
			<MetaTags>
			<title>House Account</title>
			</MetaTags>
			<div className="container-fluid">
			<Breadcrumbs title="Customers" breadcrumbItem="House account"/>
			</div>
			</div>
			</React.Fragment>	

		)
}
export default HouseAccountList;