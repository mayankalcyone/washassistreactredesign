import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const Notifications = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Mobile Configuration</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Mobile Configuration" breadcrumbItem="Notifications" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default Notifications;
