import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const PushNotificationTask = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Push Notification Task</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Mobile Configutaion" breadcrumbItem="Push Notification Task" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default PushNotificationTask;
