import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const AppSetting = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>App Setting</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Mobile Configurations" breadcrumbItem="App Setting" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default AppSetting;
