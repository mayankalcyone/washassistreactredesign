import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const CustomerFeedback = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Customer Feedback</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Mobile Configurations" breadcrumbItem="CustomerFeedback" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default CustomerFeedback;
