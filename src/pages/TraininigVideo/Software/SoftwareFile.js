import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"
const Software = (props) => {
    return (
        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Software</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Training Video" breadcrumbItem="Software" />
                </div>
            </div>
        </React.Fragment>
    );
};
export default Software;