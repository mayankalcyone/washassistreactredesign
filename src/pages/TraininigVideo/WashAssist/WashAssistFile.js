import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"
const WashAssist = (props) => {
    return (
        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>WashAssist</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Training Video" breadcrumbItem="Wash Assist" />
                </div>
            </div>
        </React.Fragment>
    );
};
export default WashAssist;