import React from 'react';
import MetaTags from 'react-meta-tags'



//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"



const Inventory = (props) => {

    return (



        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Inventory</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Inventory Types" breadcrumbItem="Inventory" />



                </div>
            </div>
        </React.Fragment>
    );
};
export default Inventory;