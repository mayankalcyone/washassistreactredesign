import React from 'react';
import MetaTags from 'react-meta-tags'



//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"



const Vendors = (props) => {

    return (



        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Vendors</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Inventory" breadcrumbItem="Vendors" />



                </div>
            </div>
        </React.Fragment>
    );
};
export default Vendors;