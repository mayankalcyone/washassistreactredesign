import React from 'react';
import MetaTags from 'react-meta-tags'



//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"



const InventoryBatches = (props) => {

    return (



        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Inventory</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Inventory Batches" breadcrumbItem="Inventory Batches" />



                </div>
            </div>
        </React.Fragment>
    );
};
export default InventoryBatches;