////import React from "react";

////const InventoryTypes = () => {
////    return (
////        <h1>InventoryTypes</h1>
////    );
////};
////export default InventoryTypes;


import React from 'react';
import MetaTags from 'react-meta-tags'



//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"



const InventoryTypes = (props) => {
    
    return (



        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Inventory Types</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Inventory" breadcrumbItem="Inventory Types" />



                </div>
            </div>
        </React.Fragment>
    );
};
export default InventoryTypes;