import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const NetworkDevices = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Network Devices</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Support" breadcrumbItem="Network Devices" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default NetworkDevices;
