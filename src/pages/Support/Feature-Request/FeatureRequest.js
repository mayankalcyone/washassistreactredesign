import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const FeatureRequest = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Feature Request</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Support" breadcrumbItem="Feature Request" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default FeatureRequest;
