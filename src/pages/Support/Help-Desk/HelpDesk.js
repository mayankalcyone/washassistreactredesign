import React from 'react';
import MetaTags from 'react-meta-tags'

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb"

const HelpDesk = (props) => {

    return (

        <React.Fragment>
            <div className="page-content">
                <MetaTags>
                    <title>Help Desk</title>
                </MetaTags>
                <div className="container-fluid">
                    <Breadcrumbs title="Support" breadcrumbItem="Help Desk" />

                </div>
            </div>
        </React.Fragment>
    );
};
export default HelpDesk;
