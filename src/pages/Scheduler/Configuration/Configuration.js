import React from 'react'
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const Configuration=()=> {
  return (
<React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Configuration</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Schedular" breadcrumbItem="Configuration" />
        </div>
    </div>
</React.Fragment>
  )
}

export default Configuration