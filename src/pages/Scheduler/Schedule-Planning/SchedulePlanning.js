import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const SchedulePlanning=()=> {
  return (
<React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Schedule Planning</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Schedular" breadcrumbItem="Schedule Planning" />
        </div>
    </div>
</React.Fragment>
  )
}

export default SchedulePlanning