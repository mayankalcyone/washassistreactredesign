import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const StaffingReports=()=> {
  return (
 <React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Staffing Reports</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Schedular" breadcrumbItem="Staffing Reports" />
        </div>
    </div>
 </React.Fragment>
  )
}

export default StaffingReports