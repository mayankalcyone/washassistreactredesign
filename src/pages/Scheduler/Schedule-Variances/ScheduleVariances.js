import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const ScheduleVariances=()=> {
  return (
<React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Schedule Variances</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Schedular" breadcrumbItem="Schedule Variances" />
        </div>
    </div>
</React.Fragment>
  )
}

export default ScheduleVariances