import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const EmployeeShiftLog=()=> {
    return (
<React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Employee Shift Log</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Schedular" breadcrumbItem="Employee Shift Log" />
        </div>
    </div>
</React.Fragment>
  )
}

export default EmployeeShiftLog