import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const AvailabilityRequest=()=> {
    return (
<React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Availability Requests</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Schedular" breadcrumbItem="Availability Requests" />
        </div>
    </div>
</React.Fragment>
  )
}

export default AvailabilityRequest