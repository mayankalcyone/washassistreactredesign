import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const EmployeeShiftApproveDeny=()=> {
  return (
<React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Employee Shift Approve/Deny</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Schedular" breadcrumbItem="Employee Shift Approve/Deny" />
        </div>
    </div>
</React.Fragment>
  )
}

export default EmployeeShiftApproveDeny