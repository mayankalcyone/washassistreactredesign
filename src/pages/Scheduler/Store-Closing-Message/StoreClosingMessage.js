import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const StoreClosingMessage=()=> {
  return (
 <React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Store Closing Message</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Schedular" breadcrumbItem="Store Closing Message" />
        </div>
    </div>
 </React.Fragment>
  )
}

export default StoreClosingMessage