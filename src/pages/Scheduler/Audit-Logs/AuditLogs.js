import React from 'react';
import MetaTags from 'react-meta-tags';
//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

const AuditLogs=()=> {
  return (
    <React.Fragment>
    <div className='page-content'>
        <MetaTags>
            <title>Audit Logs</title>
        </MetaTags>
        <div className="container-fluid">
            <Breadcrumbs title="Schedular" breadcrumbItem="Audit Logs" />
        </div>
    </div>
</React.Fragment>
  )
}

export default AuditLogs