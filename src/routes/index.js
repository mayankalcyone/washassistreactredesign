import React from "react";
import { Redirect } from "react-router-dom";

// #region WashAssist Redesign routes
//#region Schedular section start
import AuditLogs from '../pages/Scheduler/Audit-Logs/AuditLogs';
import AvailabilityRequest from '../pages/Scheduler/Availability-Request/AvailabilityRequest';
import Configuration from '../pages/Scheduler/Configuration/Configuration';
import EmployeeShiftApproveDeny from '../pages/Scheduler/Employee-Shift-Approve-Deny/EmployeeShiftApproveDeny';
import EmployeeShiftLog from '../pages/Scheduler/Employee-Shift-Log/EmployeeShiftLog';
import SchedulePlanning from '../pages/Scheduler/Schedule-Planning/SchedulePlanning';
import ScheduleVariances from '../pages/Scheduler/Schedule-Variances/ScheduleVariances';
import StaffingReports from '../pages/Scheduler/Staffing-Reports/StaffingReports';
import StoreClosingMessage from '../pages/Scheduler/Store-Closing-Message/StoreClosingMessage';
//#endregion

//#region Reports section start
import Reports from '../pages/Reports/Reports/Reports';
import ScheduleReports from '../pages/Reports/Schedule-Reports/ScheduleReports';
//#endregion

// #region Transactions Section
import Adjustments from '../pages/Transactions/Adjustments/Adjustments';
import Bankdrops from '../pages/Transactions/Banks_drops/Bankdrops';
import CreditCardTransactions from '../pages/Transactions/Credit_Card_Transactions/CreditCardTransactions';
import Payment from '../pages/Transactions/Payments/Payment';
import Payout from '../pages/Transactions/Payout/Payout';
import RefundsApproval from '../pages/Transactions/Refunds_Approval/RefundsApproval';
import RetailCustomerTransactions from '../pages/Transactions/Retail_Customer_Transactions/RetailCustomerTransactions';
import Tickets from '../pages/Transactions/Tickets/Tickets';
import TicketsDetails from '../pages/Transactions/Tickets_Details/TicketsDetails'
// #endregion

// #region Customer-Portal Section
import PortalCustomization from '../pages/Customer-Portal/Portal_Customizations/PortalCustomization';
import PortalEmailRecieptTemplates from '../pages/Customer-Portal/Portal_Email_RecieptTemplates/PortalEmailRecieptTemplates';
import PortalLocations from '../pages/Customer-Portal/Portal_Locations/PortalLocations';
import PortalNevigationMenus from '../pages/Customer-Portal/Portal_Navigation_Menus/PortalNevigationMenus';
import PortalNevigationOrderChange from '../pages/Customer-Portal/Portal_Navigation_Order_Change/PortalNevigationOrderChange';
import PortalProducts from '../pages/Customer-Portal/Portal_Products/PortalProducts';
import PortalScreenOptions from '../pages/Customer-Portal/Portal_Screen_Options/PortalScreenOptions';
import PortalSetting from '../pages/Customer-Portal/Portal_Settings/PortalSetting';
import PortalStyling from '../pages/Customer-Portal/Portal_Styling/PortalStyling';
import SideCssFile from '../pages/Customer-Portal/Site_CSS_File/SideCssFile';
import SMSTemplates from '../pages/Customer-Portal/SMS_Templates/SMSTemplates';
import TermsAndConditionAndContactInfo from '../pages/Customer-Portal/TermsAndConditionsAndContactInfo/TermsAndConditionAndContactInfo';
// #endregion

// #region Cstomer Section
import CustomerList from "../pages/Customer/Customers/CustomerList";
import GenerateHRInvoiceList from "../pages/Customer/Generate-HR-Invoices/GenerateHRInvoiceList";
import GiftCardList from "../pages/Customer/Gift-Cards/GiftCardList";
import HouseAccountList from '../pages/Customer/House-Accounts/HouseAccountList';
import LoyaltyDiscountList from '../pages/Customer/Loyalty-Discounts/LoyaltyDiscountList';
import PrepaidBookList from '../pages/Customer/Prepaid-Books/PrepaidBookList';
import RewardsAndUnlimitedProgramList from '../pages/Customer/Rewards-And-Unlimited-Programs/RewardsAndUnlimitedProgramList';
import RewardsAndUnlimitedTypeList from '../pages/Customer/Rewards-And-Unlimited-Types/RewardsAndUnlimitedTypeList';
import VehicleList from '../pages/Customer/Vehicles/VehicleList';
import RetailCustomerList from '../pages/Customer/Retail-Customers/RetailCustomerList';
import BundleList from '../pages/Customer/Bundle/BundleList';
// #endregion

// #region Configuration Section
import AccountInformationList from '../pages/Configuration/Account-Information/AccountInformationList';
import AccountTypeList from '../pages/Configuration/Account-Types/AccountTypeList';
import ButtonGroupList from '../pages/Configuration/Button-Groups/ButtonGroupList';
import ButtonTypeList from '../pages/Configuration/Button-Types/ButtonTypeList';
import CancellationReasonList from '../pages/Configuration/Cancellation-Reasons/CancellationReasonList';
import CashierButtonList from '../pages/Configuration/Cashier-Buttons/CashierButtonList';
import CategoriesList from '../pages/Configuration/Categories/CategoriesList';
import ClockInOutConfigurationList from '../pages/Configuration/Clock-In-Out-Configuration/ClockInOutConfigurationList';
import ColorList from '../pages/Configuration/Colors/ColorList';
import DashboardSettingList from '../pages/Configuration/Dashboard-Settings/DashboardSettingList';
import DayforceSettingList from '../pages/Configuration/Dayforce-Settings/DayforceSettingList';
import DepartmentList from '../pages/Configuration/Departments/DepartmentList';
import DetailTypeList from '../pages/Configuration/Detail-Types/DetailTypeList';
import DiscountList from '../pages/Configuration/Discounts/DiscountList';
import DiscountTypeList from '../pages/Configuration/Discount-Types/DiscountTypeList';
import EventTypeList from '../pages/Configuration/Event-Types/EventTypeList';
import FormCategoryList from '../pages/Configuration/Forms-Category/FormCategoryList';
import GeneralLedgersAccountCodeList from '../pages/Configuration/General-Ledgers-Account-Codes/GeneralLedgersAccountCodeList';
import GeneralLedgersCodeList from '../pages/Configuration/General-Ledgers-Codes/GeneralLedgersCodeList';
import GreetersButtonList from '../pages/Configuration/Greeters-Buttons/GreetersButtonList';
import HardwareTerminalList from '../pages/Configuration/Hardware-Terminals/HardwareTerminalList';
import ImageDesignList from '../pages/Configuration/Image-Design/ImageDesignList';
import IssueStatusTypeList from '../pages/Configuration/Issue-Status-Types/IssueStatusTypeList';
import LocationList from '../pages/Configuration/Locations/LocationList';
import LocationGroupList from '../pages/Configuration/Location-Groups/LocationGroupList';
import LoyaltyDiscountTypeList from '../pages/Configuration/Loyalty-Discount-Types/LoyaltyDiscountTypeList';
import LubeButtonList from '../pages/Configuration/Lube-Buttons/LubeButtonList';
import LubeChecklistList from '../pages/Configuration/Lube-Checklist/LubeChecklistList';
import MacrosList from '../pages/Configuration/Macros/MacrosList';
import MacrosTypeList from '../pages/Configuration/Macros-Types/MacrosTypeList';
import MacroDetailTypeList from '../pages/Configuration/Macro-Detail-Types/MacroDetailTypeList';
import PaymentTypeList from '../pages/Configuration/Payment-Types/PaymentTypeList';
import PayoutPercentageList from '../pages/Configuration/Payout-Percentage/PayoutPercentageList';
import PrepaidBookTypeList from '../pages/Configuration/Prepaid-Book-Types/PrepaidBookTypeList';
import PolicyCategoryList from '../pages/Configuration/Policy-Category/PolicyCategoryList';
import ProfitCentersList from '../pages/Configuration/Profit-Centers/ProfitCentersList';
import ProfileTypeList from '../pages/Configuration/Profile-Types/ProfileTypeList';
import RewardsList from '../pages/Configuration/Rewards/RewardsList';
import RewashReasonList from '../pages/Configuration/Rewash-Reasons/RewashReasonList';
import RefundReasonList from '../pages/Configuration/Refund-Reasons/RefundReasonList';
import ScreenLayoutList from '../pages/Configuration/Screen-Layouts/ScreenLayoutList';
import ServicesList from '../pages/Configuration/Services/ServicesList';
import ServiceTypeList from '../pages/Configuration/Service-Types/ServiceTypeList';
import ServiceGroupList from '../pages/Configuration/Service-Groups/ServiceGroupList';
import SurchargesList from '../pages/Configuration/Surcharges/SurchargesList';
import SurchargeTypeList from '../pages/Configuration/Surcharge-Types/SurchargeTypeList';
import SystemUserList from '../pages/Configuration/System-Users/SystemUserList';
import SystemTypeList from '../pages/Configuration/System-Types/SystemTypeList';
import TaxRateList from '../pages/Configuration/Tax-Rates/TaxRateList';
import TemplateList from '../pages/Configuration/Templates/TemplateList';
import TransactionTypeList from '../pages/Configuration/Transaction-Types/TransactionTypeList';
import UserLevelList from '../pages/Configuration/User-Levels/UserLevelList';
import VIPMonthlyTypeList from '../pages/Configuration/VIP-Monthly-Types/VIPMonthlyTypeList';
import VIPTypeList from '../pages/Configuration/VIP-Types/VIPTypeList';
import VIPAccountLoyaltyDiscountsList from '../pages/Configuration/VIP-Account-Loyalty-Discounts/VIPAccountLoyaltyDiscountsList';
import VIPChargeFrequencyList from '../pages/Configuration/VIP-Charge-Frequency/VIPChargeFrequencyList';
import VIPAccountServiceList from '../pages/Configuration/VIP-Account-Service/VIPAccountServiceList';
// #endregion

//#region Employee Section
import TechCommissions from "../pages/Employee/TechCommissions/TechCommissions";
import Employee from "../pages/Employee/Employee/Employee";
import EmployeeRole from "../pages/Employee/Employee-Role/EmployeRole";
import EmployeeScheduling from "../pages/Employee/Employee-Scheduling/EmployeeScheduling";
import TimeClock from "../pages/Employee/Time-Clock/TimeClock";
//#endregion

//#region PortalLink Section
import PortalLinks from "../pages/Portal-Link/PortaLinks/PortalLinks";
import PortalLinkConfiguration from "../pages/Portal-Link/PortalLinkConfigurations/PortalLinkConfigurations";

// #endregion

//#region Support Section
import HelpDesk from "../pages/Support/Help-Desk/HelpDesk";
import NetworkDevices from "../pages/Support/Network-Devices/NetworkDevices";
import FeatureRequest from "../pages/Support/Feature-Request/FeatureRequest";

// #endregion

// #region Inventory Section
import InventoryBatches from "pages/Inventory/Inventory-Batches/InventoryBatches"
import Inventory from "pages/Inventory/Inventory/Inventory";
import InventoryTypes from "pages/Inventory/Inventory-Types/InventoryTypes";
import Vendors from "pages/Inventory/Vendors/Vendors";
// #endregion

// #region Maintenance Section
import ChecklistCategory from "pages/Maintenance/Checklist-Category/ChecklistCategory";
import Checklists from "pages/Maintenance/Checklists/Checklists";
import Components from "../pages/Maintenance/Component/Components";
import EquipmentCategory from "pages/Maintenance/Equipment-Category/EquipmentCategory";
import Equipment from "pages/Maintenance/Equipment/Equipment";
import MaintenanceTemplate from "pages/Maintenance/Maintenance-Template/MaintenanceTemplate";
import Issues from "pages/Maintenance/Issues/Issues";
import Tasks from "pages/Maintenance/Tasks/Tasks";
import UserGroups from "pages/Maintenance/User-Groups/UserGroup";
import VendorsCompany from "pages/Maintenance/Vendors-Company/VendorsCompany";
// #endregion


// #region Audit Section
import Audit from '../pages/Audit/Audit/Audit';
import RefundAudit from '../pages/Audit/Refund-Audit/RefundAudit';
import TimeClockAudit from '../pages/Audit/Time-Clock-Audit/TimeClockAudit';
import LocationAudit from '../pages/Audit/Location-Audit/LocationAudit';
// #endregion

// #region DashBoard Section
import Dashboards from '../pages/Dashboard-New/Dashboard/Dashboard';
import DashboardV2 from '../pages/Dashboard-New/Dashboard-V2/Dashboard-V2';
import LocationSummary from "pages/Dashboard-New/Location-Summary/Location-Summary";
import Maintenances from "pages/Dashboard-New/Maintenance/Maintenance";
// #endregion

// #region Mobile Configutaion Section
import AppSetting from '../pages/Mobile-Configuration/App-Setting/AppSetting';
import CustomerFeedback from '../pages/Mobile-Configuration/Customer-Feedback/CustomerFeedback';
import Notifications from '../pages/Mobile-Configuration/Notifications/Notifications';
import PushNotificationType from '../pages/Mobile-Configuration/Push-Notification-Type/PushNotificationType';
import PushNotificationTask from '../pages/Mobile-Configuration/Push-Notification-Task/PushNotificationTask';

// #endregion

 



// Pages Component
import Chat from "../pages/Chat/Chat";

// Pages File Manager
import FileManager from "../pages/FileManager/index";

// Pages Calendar
import Calendar from "../pages/Calendar/index";

// User profile
import UserProfile from "../pages/Authentication/UserProfile";

//Tasks
import TasksList from "../pages/Tasks/tasks-list";
import TasksKanban from "../pages/Tasks/tasks-kanban";
import TasksCreate from "../pages/Tasks/tasks-create";

//Projects
import ProjectsGrid from "../pages/Projects/projects-grid";
import ProjectsList from "../pages/Projects/projects-list";
import ProjectsOverview from "../pages/Projects/ProjectOverview/projects-overview";
import ProjectsCreate from "../pages/Projects/projects-create";

//Ecommerce Pages
import EcommerceProducts from "../pages/Ecommerce/EcommerceProducts/index";
import EcommerceProductDetail from "../pages/Ecommerce/EcommerceProducts/EcommerceProductDetail";
import EcommerceOrders from "../pages/Ecommerce/EcommerceOrders/index";
import EcommerceCustomers from "../pages/Ecommerce/EcommerceCustomers/index";
import EcommerceCart from "../pages/Ecommerce/EcommerceCart";
import EcommerceCheckout from "../pages/Ecommerce/EcommerceCheckout";
import EcommerceShops from "../pages/Ecommerce/EcommerceShops/index";
import EcommerceAddProduct from "../pages/Ecommerce/EcommerceAddProduct";

//Email
import EmailInbox from "../pages/Email/email-inbox";
import EmailRead from "../pages/Email/email-read";
import EmailBasicTemplte from "../pages/Email/email-basic-templte";
import EmailAlertTemplte from "../pages/Email/email-template-alert";
import EmailTemplateBilling from "../pages/Email/email-template-billing";

//Invoices
import InvoicesList from "../pages/Invoices/invoices-list";
import InvoiceDetail from "../pages/Invoices/invoices-detail";

// Authentication related pages
import Login from "../pages/Authentication/Login";
import Logout from "../pages/Authentication/Logout";
import Register from "../pages/Authentication/Register";
import ForgetPwd from "../pages/Authentication/ForgetPassword";

// Inner Authentication
import Login1 from "../pages/AuthenticationInner/Login";
import Login2 from "../pages/AuthenticationInner/Login2";
import Register1 from "../pages/AuthenticationInner/Register";
import Register2 from "../pages/AuthenticationInner/Register2";
import Recoverpw from "../pages/AuthenticationInner/Recoverpw";
import Recoverpw2 from "../pages/AuthenticationInner/Recoverpw2";
import ForgetPwd1 from "../pages/AuthenticationInner/ForgetPassword";
import ForgetPwd2 from "../pages/AuthenticationInner/ForgetPwd2";
import LockScreen from "../pages/AuthenticationInner/auth-lock-screen";
import LockScreen2 from "../pages/AuthenticationInner/auth-lock-screen-2";
import ConfirmMail from "../pages/AuthenticationInner/page-confirm-mail";
import ConfirmMail2 from "../pages/AuthenticationInner/page-confirm-mail-2";
import EmailVerification from "../pages/AuthenticationInner/auth-email-verification";
import EmailVerification2 from "../pages/AuthenticationInner/auth-email-verification-2";
import TwostepVerification from "../pages/AuthenticationInner/auth-two-step-verification";
import TwostepVerification2 from "../pages/AuthenticationInner/auth-two-step-verification-2";

// Dashboard
import Dashboard from "../pages/Dashboard/index";
import DashboardSaas from "../pages/Dashboard-saas/index";
import DashboardCrypto from "../pages/Dashboard-crypto/index";
import DashboardBlog from "../pages/Dashboard-blog/index";

//Crypto
import CryptoWallet from "../pages/Crypto/CryptoWallet/crypto-wallet";
import CryptoBuySell from "../pages/Crypto/crypto-buy-sell";
import CryptoExchange from "../pages/Crypto/crypto-exchange";
import CryptoLending from "../pages/Crypto/crypto-lending";
import CryptoOrders from "../pages/Crypto/CryptoOrders/crypto-orders";
import CryptoKYCApplication from "../pages/Crypto/crypto-kyc-application";
import CryptoIcoLanding from "../pages/Crypto/CryptoIcoLanding/index";

// Charts
import ChartApex from "../pages/Charts/Apexcharts";
import ChartistChart from "../pages/Charts/ChartistChart";
import ChartjsChart from "../pages/Charts/ChartjsChart";
import EChart from "../pages/Charts/EChart";
import SparklineChart from "../pages/Charts/SparklineChart";
import ChartsKnob from "../pages/Charts/charts-knob";
import ReCharts from "../pages/Charts/ReCharts";

// Maps
import MapsGoogle from "../pages/Maps/MapsGoogle";
import MapsVector from "../pages/Maps/MapsVector";
import MapsLeaflet from "../pages/Maps/MapsLeaflet";

//Icons
import IconBoxicons from "../pages/Icons/IconBoxicons";
import IconDripicons from "../pages/Icons/IconDripicons";
import IconMaterialdesign from "../pages/Icons/IconMaterialdesign";
import IconFontawesome from "../pages/Icons/IconFontawesome";

//Tables
import BasicTables from "../pages/Tables/BasicTables";
import DatatableTables from "../pages/Tables/DatatableTables";
import ResponsiveTables from "../pages/Tables/ResponsiveTables";
import EditableTables from "../pages/Tables/EditableTables";
import DragDropTables from "../pages/Tables/DragDropTables";

// Forms
import FormElements from "../pages/Forms/FormElements/index";
import FormLayouts from "../pages/Forms/FormLayouts";
import FormAdvanced from "../pages/Forms/FormAdvanced";
import FormEditors from "../pages/Forms/FormEditors";
import FormValidations from "../pages/Forms/FormValidations";
import FormMask from "../pages/Forms/FormMask";
import FormRepeater from "../pages/Forms/FormRepeater";
import FormUpload from "../pages/Forms/FormUpload";
import FormWizard from "../pages/Forms/FormWizard";
import FormXeditable from "../pages/Forms/FormXeditable";
import DualListbox from "../pages/Forms/DualListbox";

//Ui
import UiAlert from "../pages/Ui/UiAlert";
import UiButtons from "../pages/Ui/UiButtons";
import UiCards from "../pages/Ui/UiCards";
import UiCarousel from "../pages/Ui/UiCarousel";
import UiColors from "../pages/Ui/UiColors";
import UiDropdown from "../pages/Ui/UiDropdown";
import UiGeneral from "../pages/Ui/UiGeneral";
import UiGrid from "../pages/Ui/UiGrid";
import UiImages from "../pages/Ui/UiImages";
import UiLightbox from "../pages/Ui/UiLightbox";
import UiModal from "../pages/Ui/UiModal";
import UiProgressbar from "../pages/Ui/UiProgressbar";
import UiSweetAlert from "../pages/Ui/UiSweetAlert";
import UiTabsAccordions from "../pages/Ui/UiTabsAccordions";
import UiTypography from "../pages/Ui/UiTypography";
import UiVideo from "../pages/Ui/UiVideo";
import UiSessionTimeout from "../pages/Ui/UiSessionTimeout";
import UiRating from "../pages/Ui/UiRating";
import UiRangeSlider from "../pages/Ui/UiRangeSlider";
import UiNotifications from "../pages/Ui/ui-notifications";
import UiToast from "../pages/Ui/UiToast";
import UiOffCanvas from "../pages/Ui/UiOffCanvas";
import Breadcrumb from "../pages/Ui/UiBreadcrumb";
import UiPlaceholders from "../pages/Ui/UiPlaceholders";

//Pages
import PagesStarter from "../pages/Utility/pages-starter";
import PagesMaintenance from "../pages/Utility/pages-maintenance";
import PagesComingsoon from "../pages/Utility/pages-comingsoon";
import PagesTimeline from "../pages/Utility/pages-timeline";
import PagesFaqs from "../pages/Utility/pages-faqs";
import PagesPricing from "../pages/Utility/pages-pricing";
import Pages404 from "../pages/Utility/pages-404";
import Pages500 from "../pages/Utility/pages-500";

//Contacts
import ContactsGrid from "../pages/Contacts/contacts-grid";
import ContactsList from "../pages/Contacts/ContactList/contacts-list";
import ContactsProfile from "../pages/Contacts/ContactsProfile/contacts-profile";

//Blog
import BlogList from "../pages/Blog/BlogList/index";
import BlogGrid from "../pages/Blog/BlogGrid/index";
import BlogDetails from "../pages/Blog/BlogDetails";

const authProtectedRoutes = [
    { path: "/dashboard", component: Dashboard },
    { path: "/dashboard-saas", component: DashboardSaas },
    { path: "/dashboard-crypto", component: DashboardCrypto },
    { path: "/dashboard-blog", component: DashboardBlog },


// #region Transactions Section
    { path: "/adjustment", component: Adjustments },
    { path: "/bankdrops", component: Bankdrops },
    { path: "/credit-card-transactions", component: CreditCardTransactions },
    { path: "/payment", component: Payment },
    { path: "/refund-approval", component: RefundsApproval },
    { path: "/retail-customer-transactions", component: RetailCustomerTransactions },
    { path: "/tickets", component: Tickets },
    { path: "/tickets-details", component: TicketsDetails },
    { path: "/payouts", component: Payout },
// #endregion

// #region New Dashboard Section
    { path: "/dashboards", component: Dashboards },
    { path: "/dashboard-v2", component: DashboardV2 },
    { path: "/location-summary", component: LocationSummary },
    { path: "/maintenance", component: Maintenances },
// #endregion
    //#region Schedular section start
    { path: "/auditlogs", component: AuditLogs },
    { path: "/availability-request", component: AvailabilityRequest },
    { path: "/configuration", component: Configuration },
    { path: "/employee-shift-approve-deny", component: EmployeeShiftApproveDeny },
    { path: "/employee-shift-log", component: EmployeeShiftLog },
    { path: "/schedule-planning", component: SchedulePlanning },
    { path: "/schedule-variances", component: ScheduleVariances },
    { path: "/staffing-reports", component: StaffingReports },
    { path: "/store-closing-message", component: StoreClosingMessage },
    //#endregion

    //#region Reports section start
    { path: "/reports", component: Reports },
    { path: "/schedule-reports", component: ScheduleReports },
//#endregion

// #region Customer-Portal Section
    { path: "/portal-customization", component: PortalCustomization },
    { path: "/portal-email-reciept-templetes", component: PortalEmailRecieptTemplates },
    { path: "/portal-locations", component: PortalLocations },
    { path: "/portal-nevigation-menus", component: PortalNevigationMenus },
    { path: "/portal-nevigation-order-change", component: PortalNevigationOrderChange },
    { path: "/portal-porducts", component: PortalProducts },
    { path: "/portal-screen-options", component: PortalScreenOptions },
    { path: "/portal-settings", component: PortalSetting },
    { path: "/portal-styling", component: PortalStyling },
    { path: "/side-css-file", component: SideCssFile },
    { path: "/sms-templates", component: SMSTemplates },
    { path: "/terms-and-condition-and-contactinfo", component: TermsAndConditionAndContactInfo },
// #endregion

// #region CstomerModule
    { path: "/customer-list", component: CustomerList },
    { path: "/generate-hr-invoice-list", component: GenerateHRInvoiceList },
    { path: "/gift-card-list", component: GiftCardList },
    { path: "/house-account-list", component: HouseAccountList },
    { path: "/loyalty-discount-list", component: LoyaltyDiscountList },
    { path: "/prepaid-book-list", component: PrepaidBookList },
    { path: "/rewards-and-unlimited-program-list", component: RewardsAndUnlimitedProgramList },
    { path: "/rewards-and-unlimited-type-list", component: RewardsAndUnlimitedTypeList },
    { path: "/vehicle-list", component: VehicleList },
    { path: "/retail-customer-list", component: RetailCustomerList },
    { path: "/bundle-list", component: BundleList },
// #endregion

// #region ConfigurationModule
    { path: "/account-information-list", component: AccountInformationList },
    { path: "/account-type-list", component: AccountTypeList },
    { path: "/button-group-list", component: ButtonGroupList },
    { path: "/button-type-list", component: ButtonTypeList },
    { path: "/cancellation-reason-list", component: CancellationReasonList },
    { path: "/cashier-button-list", component: CashierButtonList },
    { path: "/categories-list", component: CategoriesList },
    { path: "/clock-in-out-configuration-list", component: ClockInOutConfigurationList },
    { path: "/color-list", component: ColorList },
    { path: "/dashboard-setting-list", component: DashboardSettingList },
    { path: "/dayforce-setting-list", component: DayforceSettingList },
    { path: "/department-list", component: DepartmentList },
    { path: "/detail-type-list", component: DetailTypeList },
    { path: "/discount-list", component: DiscountList },
    { path: "/discount-type-list", component: DiscountTypeList },
    { path: "/event-type-list", component: EventTypeList },
    { path: "/form-category-list", component: FormCategoryList },
    { path:"/general-ledgers-account-code-list", component:GeneralLedgersAccountCodeList},
    { path:"/general-ledgers-code-list", component:GeneralLedgersCodeList},
    { path:"/greeters-button-list", component:GreetersButtonList},
    { path:"/hardware-terminal-list",component:HardwareTerminalList},
    { path:"/image-design-list",component:ImageDesignList},
    { path:"/issue-status-type-list",component:IssueStatusTypeList},
    { path:"/location-list",component:LocationList},
    { path:"/location-group-list",component:LocationGroupList},
    { path:"/loyalty-discount-type-list",component:LoyaltyDiscountTypeList},
    { path:"/lube-button-list",component:LubeButtonList},
    { path:"/lube-checklist-list",component:LubeChecklistList},
    { path:"/macros-list",component:MacrosList},
    { path:"/macros-type-list",component:MacrosTypeList},
    { path:"/macro-detail-type-list",component:MacroDetailTypeList},
    { path:"/payment-type-list",component:PaymentTypeList},
    { path:"/payout-percentage-list",component:PayoutPercentageList},
    { path:"/prepaid-book-type-list",component:PrepaidBookTypeList},
    { path:"/policy-category-list",component:PolicyCategoryList},
    { path:"/profit-centers-list",component:ProfitCentersList},
    { path:"/profile-type-list",component:ProfileTypeList},
    { path:"/rewards-list",component:RewardsList},
    { path:"/rewash-reason-list",component:RewashReasonList},
    { path:"/refund-reason-list",component:RefundReasonList},
    { path:"/screen-layout-list",component:ScreenLayoutList},
    { path:"/services-list",component:ServicesList},
    { path:"/service-type-list",component:ServiceTypeList},
    { path:"/service-group-list",component:ServiceGroupList},
    { path:"/surcharges-list",component:SurchargesList},
    { path:"/surcharge-type-list",component:SurchargeTypeList},
    { path:"/system-user-list",component:SystemUserList},
    { path:"/system-type-list",component:SystemTypeList},
    { path:"/tax-rate-list",component:TaxRateList},
    { path:"/template-list",component:TemplateList},
    { path:"/transaction-type-list",component:TransactionTypeList},
    { path:"/user-level-list",component:UserLevelList},
    { path:"/vip-monthly-type-list",component:VIPMonthlyTypeList},
    { path:"/vip-type-list",component:VIPTypeList},
    { path:"/vip-account-loyalty-discounts-list",component:VIPAccountLoyaltyDiscountsList},
    { path:"/vip-charge-frequency-list",component:VIPChargeFrequencyList},
    { path:"/vip-account-service-list",component:VIPAccountServiceList},

// #endregion

// #region Emoloye Section
    { path: "/tech-commissions", component: TechCommissions },
    { path: "/employee", component: Employee },
    { path: "/employee-role", component: EmployeeRole },
    { path: "/employee-scheduling", component: EmployeeScheduling },
    { path: "/time-clock", component: TimeClock },
// #endregion

// #region Portal Link section
    { path: "/portal-links", component: PortalLinks },
    { path: "/portal-link-configuration", component: PortalLinkConfiguration },

// #endregion

// #region Support Section
    { path: "/help-desk", component: HelpDesk },
    { path: "/network-devices", component: NetworkDevices },
    { path: "/feature-request", component: FeatureRequest },

// #endregion

// #region InventorySection
    { path: "/inventory-batches", component: InventoryBatches },
    { path: "/inventory", component: Inventory },
    { path: "/inventory-types", component: InventoryTypes },
    { path: "/vendors", component: Vendors },
// #endregion

// #region MaintenanceSection
    { path: "/checklist-category", component: ChecklistCategory },
    { path: "/checklists", component: Checklists },
    { path: "/components", component: Components },
    { path: "/equipment-category", component: EquipmentCategory },
    { path: "/equipment", component: Equipment },
    { path: "/maintenance-template", component: MaintenanceTemplate },
    { path: "/issues", component: Issues },
    { path: "/tasks", component: Tasks },
    { path: "/user-groups", component: UserGroups },
    { path: "/vendors-company", component: VendorsCompany },
// #endregion


 // #region Mobile Configutaion Section
{ path: "/app-setting", component: AppSetting },
{ path: "/customer-feedback", component: CustomerFeedback },
{ path: "/notifications", component: Notifications },
{ path: "/push-notification-type", component: PushNotificationType },
{ path: "/push-notification-task", component: PushNotificationTask },
// #endregion

 // #region Audit Section
    { path: "/audit", component: Audit },
    { path: "/refund-audit", component: RefundAudit },
    { path: "/time-clock-audit", component: TimeClockAudit },
    { path: "/location-audit", component: LocationAudit },
// #endregion


  { path: "/dashboard", component: Dashboard },
  { path: "/dashboard-saas", component: DashboardSaas },
  { path: "/dashboard-crypto", component: DashboardCrypto },
  { path: "/dashboard-blog", component: DashboardBlog },

  //Crypto
  { path: "/crypto-wallet", component: CryptoWallet },
  { path: "/crypto-buy-sell", component: CryptoBuySell },
  { path: "/crypto-exchange", component: CryptoExchange },
  { path: "/crypto-lending", component: CryptoLending },
  { path: "/crypto-orders", component: CryptoOrders },
  { path: "/crypto-kyc-application", component: CryptoKYCApplication },

  //profile
  { path: "/profile", component: UserProfile },

  //chat
  { path: "/chat", component: Chat },

  //File Manager
  { path: "/apps-filemanager", component: FileManager },

  //calendar
  { path: "/calendar", component: Calendar },

  //Ecommerce
  // { path: "/ecommerce-products/:id", component: EcommerceProducts },
  { path: "/ecommerce-products", component: EcommerceProducts },
  { path: "/ecommerce-product-details/:id", component: EcommerceProductDetail },

  { path: "/ecommerce-orders", component: EcommerceOrders },
  { path: "/ecommerce-customers", component: EcommerceCustomers },
  { path: "/ecommerce-cart", component: EcommerceCart },
  { path: "/ecommerce-checkout", component: EcommerceCheckout },
  { path: "/ecommerce-shops", component: EcommerceShops },
  { path: "/ecommerce-add-product", component: EcommerceAddProduct },

  //Email
  { path: "/email-inbox", component: EmailInbox },
  { path: "/email-read", component: EmailRead },
  { path: "/email-template-basic", component: EmailBasicTemplte },
  { path: "/email-template-alert", component: EmailAlertTemplte },
  { path: "/email-template-billing", component: EmailTemplateBilling },

  //Invoices
  { path: "/invoices-list", component: InvoicesList },
  { path: "/invoices-detail", component: InvoiceDetail },
  { path: "/invoices-detail/:id", component: InvoiceDetail },

  // Tasks
  { path: "/tasks-list", component: TasksList },
  { path: "/tasks-kanban", component: TasksKanban },
  { path: "/tasks-create", component: TasksCreate },

  //Projects
  { path: "/projects-grid", component: ProjectsGrid },
  { path: "/projects-list", component: ProjectsList },
  { path: "/projects-overview", component: ProjectsOverview },
  { path: "/projects-overview/:id", component: ProjectsOverview },
  { path: "/projects-create", component: ProjectsCreate },

  // Contacts
  { path: "/contacts-grid", component: ContactsGrid },
  { path: "/contacts-list", component: ContactsList },
  { path: "/contacts-profile", component: ContactsProfile },

  //Blog
  { path: "/blog-list", component: BlogList },
  { path: "/blog-grid", component: BlogGrid },
  { path: "/blog-details", component: BlogDetails },

  //Charts
  { path: "/apex-charts", component: ChartApex },
  { path: "/chartist-charts", component: ChartistChart },
  { path: "/chartjs-charts", component: ChartjsChart },
  { path: "/e-charts", component: EChart },
  { path: "/sparkline-charts", component: SparklineChart },
  { path: "/charts-knob", component: ChartsKnob },
  { path: "/re-charts", component: ReCharts },

  // Icons
  { path: "/icons-boxicons", component: IconBoxicons },
  { path: "/icons-dripicons", component: IconDripicons },
  { path: "/icons-materialdesign", component: IconMaterialdesign },
  { path: "/icons-fontawesome", component: IconFontawesome },

  // Tables
  { path: "/tables-basic", component: BasicTables },
  { path: "/tables-datatable", component: DatatableTables },
  { path: "/tables-responsive", component: ResponsiveTables },
  { path: "/tables-editable", component: EditableTables },
  { path: "/tables-dragndrop", component: DragDropTables },

  // Maps
  { path: "/maps-google", component: MapsGoogle },
  { path: "/maps-vector", component: MapsVector },
  { path: "/maps-leaflet", component: MapsLeaflet },

  // Forms
  { path: "/form-elements", component: FormElements },
  { path: "/form-layouts", component: FormLayouts },
  { path: "/form-advanced", component: FormAdvanced },
  { path: "/form-editors", component: FormEditors },
  { path: "/form-mask", component: FormMask },
  { path: "/form-repeater", component: FormRepeater },
  { path: "/form-uploads", component: FormUpload },
  { path: "/form-wizard", component: FormWizard },
  { path: "/form-validation", component: FormValidations },
  { path: "/form-xeditable", component: FormXeditable },
  { path: "/dual-listbox", component: DualListbox },

  // Ui
  { path: "/ui-alerts", component: UiAlert },
  { path: "/ui-buttons", component: UiButtons },
  { path: "/ui-cards", component: UiCards },
  { path: "/ui-carousel", component: UiCarousel },
  { path: "/ui-colors", component: UiColors },
  { path: "/ui-dropdowns", component: UiDropdown },
  { path: "/ui-general", component: UiGeneral },
  { path: "/ui-grid", component: UiGrid },
  { path: "/ui-images", component: UiImages },
  { path: "/ui-lightbox", component: UiLightbox },
  { path: "/ui-modals", component: UiModal },
  { path: "/ui-progressbars", component: UiProgressbar },
  { path: "/ui-sweet-alert", component: UiSweetAlert },
  { path: "/ui-tabs-accordions", component: UiTabsAccordions },
  { path: "/ui-typography", component: UiTypography },
  { path: "/ui-video", component: UiVideo },
  { path: "/ui-session-timeout", component: UiSessionTimeout },
  { path: "/ui-rating", component: UiRating },
  { path: "/ui-rangeslider", component: UiRangeSlider },
  { path: "/ui-notifications", component: UiNotifications },
  { path: "/ui-toasts", component: UiToast },
  { path: "/ui-offcanvas", component: UiOffCanvas },
  { path: "/ui-breadcrumb", component: Breadcrumb },
  { path: "/ui-placeholders", component: UiPlaceholders },
  //Utility
  { path: "/pages-starter", component: PagesStarter },
  { path: "/pages-timeline", component: PagesTimeline },
  { path: "/pages-faqs", component: PagesFaqs },
  { path: "/pages-pricing", component: PagesPricing },

  // this route should be at the end of all other routes
  // eslint-disable-next-line react/display-name
  { path: "/", exact: true, component: () => <Redirect to="/dashboard" /> },
];

const publicRoutes = [
  { path: "/logout", component: Logout },
  { path: "/login", component: Login },
  { path: "/forgot-password", component: ForgetPwd },
  { path: "/register", component: Register },

  { path: "/pages-maintenance", component: PagesMaintenance },
  { path: "/pages-comingsoon", component: PagesComingsoon },
  { path: "/pages-404", component: Pages404 },
  { path: "/pages-500", component: Pages500 },
  { path: "/crypto-ico-landing", component: CryptoIcoLanding },

  // Authentication Inner
  { path: "/pages-login", component: Login1 },
  { path: "/pages-login-2", component: Login2 },

  { path: "/pages-register", component: Register1 },
  { path: "/pages-register-2", component: Register2 },

  { path: "/page-recoverpw", component: Recoverpw },
  { path: "/pages-recoverpw-2", component: Recoverpw2 },

  { path: "/pages-forgot-pwd", component: ForgetPwd1 },
  { path: "/pages-forgot-pwd-2", component: ForgetPwd2 },

  { path: "/auth-lock-screen", component: LockScreen },
  { path: "/auth-lock-screen-2", component: LockScreen2 },
  { path: "/page-confirm-mail", component: ConfirmMail },
  { path: "/page-confirm-mail-2", component: ConfirmMail2 },
  { path: "/auth-email-verification", component: EmailVerification },
  { path: "/auth-email-verification-2", component: EmailVerification2 },
  { path: "/auth-two-step-verification", component: TwostepVerification },
  { path: "/auth-two-step-verification-2", component: TwostepVerification2 },
];

export { authProtectedRoutes, publicRoutes };