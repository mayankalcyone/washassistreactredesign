import PropTypes from "prop-types";
import React, { Component } from "react";

//Simple bar
import SimpleBar from "simplebar-react";

// MetisMenu
import MetisMenu from "metismenujs";
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";

//i18n
import { withTranslation } from "react-i18next";

class SidebarContent extends Component {
    constructor(props) {
        super(props);
        this.refDiv = React.createRef();
    }

    componentDidMount() {
        this.initMenu();
    }

    // eslint-disable-next-line no-unused-vars
    componentDidUpdate(prevProps, prevState, ss) {
        if (this.props.type !== prevProps.type) {
            this.initMenu();
        }
    }

    initMenu() {
        new MetisMenu("#side-menu");

        let matchingMenuItem = null;
        const ul = document.getElementById("side-menu");
        const items = ul.getElementsByTagName("a");
        for (let i = 0; i < items.length; ++i) {
            if (this.props.location.pathname === items[i].pathname) {
                matchingMenuItem = items[i];
                break;
            }
        }
        if (matchingMenuItem) {
            this.activateParentDropdown(matchingMenuItem);
        }
    }

    // componentDidUpdate() {}

    scrollElement = item => {
        setTimeout(() => {
            if (this.refDiv.current !== null) {
                if (item) {
                    const currentPosition = item.offsetTop;
                    if (currentPosition > window.innerHeight) {
                        if (this.refDiv.current)
                            this.refDiv.current.getScrollElement().scrollTop =
                                currentPosition - 300;
                    }
                }
            }
        }, 300);
    };

    activateParentDropdown = item => {
        item.classList.add("active");
        const parent = item.parentElement;

        const parent2El = parent.childNodes[1];
        if (parent2El && parent2El.id !== "side-menu") {
            parent2El.classList.add("mm-show");
        }

        if (parent) {
            parent.classList.add("mm-active");
            const parent2 = parent.parentElement;

            if (parent2) {
                parent2.classList.add("mm-show"); // ul tag

                const parent3 = parent2.parentElement; // li tag

                if (parent3) {
                    parent3.classList.add("mm-active"); // li
                    parent3.childNodes[0].classList.add("mm-active"); //a
                    const parent4 = parent3.parentElement; // ul
                    if (parent4) {
                        parent4.classList.add("mm-show"); // ul
                        const parent5 = parent4.parentElement;
                        if (parent5) {
                            parent5.classList.add("mm-show"); // li
                            parent5.childNodes[0].classList.add("mm-active"); // a tag
                        }
                    }
                }
            }
            this.scrollElement(item);
            return false;
        }
        this.scrollElement(item);
        return false;
    };

    render() {
        return (
            <React.Fragment>
                <SimpleBar className="h-100" ref={this.refDiv}>
                    <div id="sidebar-menu">
                        <ul className="metismenu list-unstyled" id="side-menu">
                            <li className="menu-title">{this.props.t("Menu")}</li>
                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bx-home-circle" />
                                    <span className="badge rounded-pill bg-info float-end">
                                        {/* 04 */}
                                    </span>
                                    <span>{this.props.t("Dashboards")}</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/dashboard">{this.props.t("Default")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/dashboard-saas">{this.props.t("Saas")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/dashboard-crypto">{this.props.t("Crypto")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/dashboard-blog">{this.props.t("Blog")}</Link>
                                    </li>
                                </ul>
                            </li>
                            {/*New Dashboard Module Start */}

                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="fas fa-tv" />
                                    <span>Dashboard</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/maintenance">
                                           <i className="bx bx-bar-chart-alt" />
                                            Maintenance
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/location-summary">
                                            <i className="fas fa-map-marker-alt" />
                                            Location Summary
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/dashboards">
                                            <i className="fas fa-tv" />
                                            Dashboard
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/dashboard-v2">
                                            <i className="fas fa-tv" />
                                            Dashboard V2
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            {/* New Dashboard Module End*/}
                            {/*Customer Navigation starts */}
                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="fas fa-users" />
                                    <span>Customers</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/customer-list">
                                            <i className="fas fa-users" />
                                            Customers
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/generate-hr-invoice-list">
                                            <i className="dripicons-document" />
                                            Generate HR Invoices
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/gift-card-list">
                                            <i className="far fa-address-card" />
                                            Gift Cards
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/house-account-list">
                                            <i className="bx bxs-home" />
                                            House Accounts
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/loyalty-discount-list">
                                            <i className="fas fa-chevron-circle-down" />
                                            Loyalty Discounts
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/prepaid-book-list">
                                            <i className="bx bx-credit-card" />
                                            Prepaid Books
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/rewards-and-unlimited-program-list">
                                            <i className="bx bx-star" />
                                            Rewards and Unlimited Programs
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/rewards-and-unlimited-type-list">
                                            <i className="bx bxs-star" />
                                            Rewards and Unlimited Type
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/vehicle-list">
                                            <i className="fas fa-car-side" />
                                            Vehicles
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/retail-customer-list">
                                            <i className="fas fa-users" />
                                            Retail Customers
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/bundle-list">
                                            <i className="bx bx-list-ul" />
                                            Bundle
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            {/* Customer Navigation ends*/}

                            {/*Inventory Module Start */}
                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bx-list-ul" />
                                    <span>Inventory</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/inventory-batches">
                                            <i className="dripicons-user-group" />
                                            InventoryBatches
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/inventory">
                                            <i className="bx bx-sitemap" />
                                            Inventory
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/inventory-types">
                                            <i className="bx bx-layer" />
                                            InventoryTypes
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/vendors">
                                            <i className="bx bx-book-open" />
                                            Vendors
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            {/* Inventory Module End*/}

                            {/*Transactions Section Start*/}
                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bx-briefcase" />
                                    <span>Transactions</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/adjustment">
                                            <i className="bx bx-layer" />
                                            Adjustments
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/bankdrops">
                                            <i className="dripicons-document" />
                                            Bank Drops
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/credit-card-transactions">
                                            <i className="bx bxs-credit-card" />
                                            Credit Card Transactions
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/payouts">
                                            <i className="fas fa-camera-retro" />
                                            Payouts
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/payment">
                                            <i className="fas fa-search" />
                                            Payments
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/tickets">
                                            <i className="fas fa-ticket-alt" />
                                            Tickets
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/tickets-details">
                                            <i className="fas fa-search" />
                                            Tickets Details
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/retail-customer-transactions">
                                            <i className="bx bxs-credit-card" />
                                            Retail Customer Transactions
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/refund-approval">
                                            <i className="bx bxs-dollar-circle" />
                                            Refunds Approval
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            {/*Transactions Section End*/}

                            {/*EmoloyeSection Start*/}
                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="fas fa-user" />
                                    <span>Employees</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/tech-commissions">
                                            <i className="bx bxs-pie-chart-alt-2" />
                                            Tech Commissions
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/employee">
                                            <i className="dripicons-user-group" />
                                            Employees
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/employee-role">
                                            <i className="bx bx-list-ul" />
                                            Employee Roles
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/employee-scheduling">
                                            <i className="bx bx-calendar" />
                                            Employee Scheduling
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/time-clock">
                                            <i className="fas fa-chevron-circle-left" />
                                            Time Clock
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            {/*EmoloyeSection End*/}

                            {/*Maintenance Module Start*/}
                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="far fa-sun" />
                                    <span>Maintenance</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/checklist-category">
                                            <i className="bx bx-list-ul" />
                                            Checklist Category
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/checklists">
                                            <i className="bx bx-calendar-check" />
                                            Checklists
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/components">
                                            <i className="bx bx-list-ul" />
                                            Components
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/equipment-category">
                                            <i className="bx bx-list-ul" />
                                            Equipment Category
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/equipment">
                                            <i className="far fa-sun" />
                                            Equipment
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/maintenance-template">
                                            <i className="far fa-sun" />
                                            Maintenance Template
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/issues">
                                            <i className="fas fa-spider" />
                                            Issues
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/tasks">
                                            <i className="bx bx-select-multiple" />
                                            Tasks
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/user-groups">
                                            <i className="dripicons-user-group" />
                                            User Groups
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/vendors-company">
                                            <i className="bx bx-store-alt" />
                                            Vendor Company
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            {/* Maintenance Module End*/}

                            {/*Configuration module starts*/}
                        <li>
                            <Link to="/#" className="has-arrow">
                                <i className="bx bx-home-circle" />
                                <span>Configuration</span>
                            </Link>
                            <ul className="sub-menu" aria-expanded="false">
                                <li>
                                    <Link to="/account-information-list">
                                        Account Information
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/account-type-list">
                                        Account Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/button-group-list">
                                        Button Groups
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/button-type-list">
                                        Button Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/cancellation-reason-list">
                                        Cancellation Reasons
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/cashier-button-list">
                                        Cashier Buttons
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/categories-list">
                                        Categories
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/clock-in-out-configuration-list">
                                        Clock In/Out Configurations
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/color-list">
                                        Colors
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/dashboard-setting-list">
                                        Dashboard Settings
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/dayforce-setting-list">
                                        Dayforce Settings
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/department-list">
                                        Departments
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/detail-type-list">
                                        Detail Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/discount-list">
                                        Discounts
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/discount-type-list">
                                        Discounts Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/event-type-list">
                                        Event Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/form-category-list">
                                        Forms Category
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/general-ledgers-account-code-list">
                                        General Ledgers
                                        Account Codes
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/general-ledgers-code-list">
                                        General Ledgers Codes
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/greeters-button-list">
                                        Greeters Buttons
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/hardware-terminal-list">
                                        Hardware/Terminals
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/image-design-list">
                                        Image Design
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/issue-status-type-list">
                                        Issue Status Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/location-list">
                                        Locations
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/location-group-list">
                                        Location Groups
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/loyalty-discount-type-list">
                                        Loyalty Discount Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/lube-button-list">
                                        Lube Buttons
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/lube-checklist-list">
                                        Lube Checklists
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/macros-list">
                                        Macros
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/macros-type-list">
                                        Macro Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/macro-detail-type-list">
                                        Macro Detail Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/payment-type-list">
                                        Payment Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/payout-percentage-list">
                                        Payout Percentage
                                    </Link>
                                </li>
                                
                                <li>
                                    <Link to="/prepaid-book-type-list">
                                        Prepaid Book Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/policy-category-list">
                                        Policy Category
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/profit-centers-list">
                                        Profit Centers
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/profile-type-list">
                                        Profile Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/rewards-list">
                                        Rewards
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/rewash-reason-list">
                                        Rewash Reasons
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/refund-reason-list">
                                        Refund Reasons
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/screen-layout-list">
                                        Screen Layouts
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/services-list">
                                        Services
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/service-type-list">
                                        Service Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/service-group-list">
                                        Service Groups
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/surcharges-list">
                                        Surcharges
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/surcharge-type-list">
                                        Surcharge Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/system-user-list">
                                        System Users
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/system-type-list">
                                        System Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/tax-rate-list">
                                        Tax Rates
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/template-list">
                                        Templates
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/transaction-type-list">
                                        Transaction Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/user-level-list">
                                        User Levels
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/vip-monthly-type-list">
                                        VIP Monthly Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/vip-type-list">
                                        VIP Types
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/vip-account-loyalty-discounts-list">
                                        VIP Account Loyalty Discounts
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/vip-charge-frequency-list">
                                        VIP Charge Frequency
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/vip-account-service-list">
                                        VIP Account Srevice
                                    </Link>
                                </li>
                            </ul>
                        </li>
                        {/*Configuration module ends*/}

                            {/*Customer Portal Start*/}
                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="far fa-sun" />
                                    <span>Customer Portal</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/portal-styling">
                                            <i className="fab fa-product-hunt" />
                                            Portal Styling
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/portal-porducts">
                                            <i className="fab fa-product-hunt" />
                                            Portal Products
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/portal-nevigation-menus">
                                            <i className="bx bx-menu" />
                                            Portal Navigation Menus
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/portal-nevigation-order-change">
                                            <i className="bx bx-list-ul" />
                                            Portal Navigation Order Change
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/portal-email-reciept-templetes">
                                            <i className="bx bxl-pinterest-alt" />
                                            Portal Email Reciept Templates
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/portal-customization">
                                            <i className="bx bxl-pinterest-alt" />
                                            Portal Customizations
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/portal-settings">
                                            <i className="bx bxl-pinterest-alt" />
                                            Portal Settings
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/side-css-file">
                                            <i className="bx bx-menu" />
                                            Site CSS File
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/portal-screen-options">
                                            <i className="bx bxl-pinterest-alt" />
                                            Portal Screen Options
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/sms-templates">
                                            <i className="bx bxl-pinterest-alt" />
                                            SMS Templates
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/terms-and-condition-and-contactinfo">
                                            <i className="bx bxl-pinterest-alt" />
                                            Terms and Conditions & Contact Info
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/portal-locations">
                                            <i className="bx bxl-pinterest-alt" />
                                            Portal Locations
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            {/*Customer Portal End*/}

                            {/*Portal Link Start*/}
                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="far fa-sun" />
                                    <span>Portal Link</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/portal-links">
                                            <i className="fab fa-product-hunt" />
                                            Portal Links
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/portal-link-configuration">
                                            <i className="fab fa-product-hunt" />
                                            Portal Links Configuration
                                        </Link>
                                    </li>
                                </ul>
                            </li>

                            {/*Portal Link End*/}

                            {/*Support Start*/}
                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="dripicons-web" />
                                    <span>Support</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/help-desk">
                                            <i className="bx bx-layer" />
                                            Help Desk
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/network-devices">
                                            <i className="bx bx-globe" />
                                            Network Devices
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/feature-request">
                                            <i className="dripicons-to-do" />
                                            Feature Request
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            {/*Support Start*/}

                            {/*Mobile Configuration Start*/}
                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="fas fa-mobile-alt" />
                                    <span>Mobile Configuration</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/notifications">
                                            <i className="bx bxs-bell-ring" />
                                            Notifications
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/customer-feedback">
                                            <i className="bx bx-star" />
                                            Customer Feedback
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/push-notification-type">
                                            <i className="fas fa-ticket-alt" />
                                            Push Notification Type
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/push-notification-task">
                                            <i className="fas fa-tasks" />
                                            Push Notification Task
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/app-setting">
                                            <i className="bx bxs-cog" />
                                            App Settings
                                        </Link>
                                    </li>
                                </ul>
                            </li>

                            {/*Mobile Configuration End*/}
                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="dripicons-document" />
                                    <span>Audit</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/">
                                            <i className="bx bx-file" />
                                            Audit
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/">
                                            <i className="bx bx-file" />
                                            Refund Audit
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/">
                                            <i className="bx bx-file" />
                                            Time Clock Audit
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/">
                                            <i className="bx bx-file" />
                                            Location Audit
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="fas fa-camera-retro" />
                                    <span>Scheduler</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/auditlogs">
                                            <i className="dripicons-user-group" />
                                            Audit Logs
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/availability-reques">
                                            <i className="dripicons-user-group" />
                                            Availability Request
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/configuration">
                                            <i className="bx bxs-cog" />
                                            Configuration
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/employee-shift-approve-deny">
                                            <i className="bx bxs-news" />
                                            Employee Shift Approve/Deny
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/employee-shift-log">
                                            <i className="bx bxs-news" />
                                            Employee Shift Log
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/schedule-planning">
                                            <i className="bx bxs-cylinder" />
                                            Schedule Planning
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/staffing-reports">
                                            <i className="bx bx-file" />
                                            Staffing Report
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/store-closing-message">
                                            <i className="bx bx-file" />
                                            Store Closing Message
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/schedule-variances">
                                            <i className="bx bx-file" />
                                            Schedule Variances
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="dripicons-document" />
                                    <span>Reports</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/reports">
                                            <i className="bx bx-file" />
                                            Reports
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/schedule-reports">
                                            <i className="bx bx-file" />
                                            Schedule Reports
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <Link to="/#">
                                    <i className="far fa-id-card" />
                                    <span>User Guide</span>
                                </Link>
                            </li>
                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bx-home-circle" />
                                    <span>Training Video</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/wash-assist">
                                            WashAssist
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/software">
                                            Software
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            <li className="menu-title">{this.props.t("Apps")}</li>

                            <li>
                                <Link to="/calendar" className="">
                                    <i className="bx bx-calendar" />
                                    <span>{this.props.t("Calendar")}</span>
                                </Link>
                            </li>

                            <li>
                                <Link to="/chat" className="">
                                    <i className="bx bx-chat" />
                                    <span>{this.props.t("Chat")}</span>
                                </Link>
                            </li>
                            <li>
                                <Link to="/apps-filemanager" className="">
                                    <i className="bx bx-file" />
                                    <span className="badge rounded-pill bg-success float-end">
                                        {this.props.t("New")}
                                    </span>
                                    <span>{this.props.t("File Manager")}</span>
                                </Link>
                            </li>

                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bx-store" />
                                    <span>{this.props.t("Ecommerce")}</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/ecommerce-products">
                                            {this.props.t("Products")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/ecommerce-product-details/1">
                                            {this.props.t("Product Details")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/ecommerce-orders">{this.props.t("Orders")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ecommerce-customers">
                                            {this.props.t("Customers")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/ecommerce-cart">{this.props.t("Cart")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ecommerce-checkout">
                                            {this.props.t("Checkout")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/ecommerce-shops">{this.props.t("Shops")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ecommerce-add-product">
                                            {this.props.t("Add Product")}
                                        </Link>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bx-bitcoin" />
                                    <span>{this.props.t("Crypto")}</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/crypto-wallet">{this.props.t("Wallet")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/crypto-buy-sell">
                                            {this.props.t("Buy/Sell")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/crypto-exchange">
                                            {this.props.t("Exchange")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/crypto-lending">{this.props.t("Lending")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/crypto-orders">{this.props.t("Orders")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/crypto-kyc-application">
                                            {this.props.t("KYC Application")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/crypto-ico-landing">
                                            {this.props.t("ICO Landing")}
                                        </Link>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bx-envelope"></i>
                                    <span>{this.props.t("Email")}</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/email-inbox">{this.props.t("Inbox")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/email-read">{this.props.t("Read Email")} </Link>
                                    </li>
                                    <li>
                                        <Link to="/#">
                                            <span
                                                className="badge rounded-pill badge-soft-success float-end"
                                                key="t-new"
                                            >
                                                {this.props.t("New")}
                                            </span>
                                            <span key="t-email-templates">
                                                {this.props.t("Templates")}
                                            </span>
                                        </Link>
                                        <ul className="sub-menu" aria-expanded="false">
                                            <li>
                                                <Link to="/email-template-basic">
                                                    {this.props.t("Basic Action")}
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/email-template-alert">
                                                    {this.props.t("Alert Email")}{" "}
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/email-template-billing">
                                                    {this.props.t("Billing Email")}{" "}
                                                </Link>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bx-receipt" />
                                    <span>{this.props.t("Invoices")}</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/invoices-list">
                                            {this.props.t("Invoice List")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/invoices-detail">
                                            {this.props.t("Invoice Detail")}
                                        </Link>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bx-briefcase-alt-2" />
                                    <span>{this.props.t("Projects")}</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/projects-grid">
                                            {this.props.t("Projects Grid")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/projects-list">
                                            {this.props.t("Projects List")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/projects-overview">
                                            {this.props.t("Project Overview")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/projects-create">
                                            {this.props.t("Create New")}
                                        </Link>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bx-task" />
                                    <span>{this.props.t("Tasks")}</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/tasks-list">{this.props.t("Task List")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/tasks-kanban">
                                            {this.props.t("Kanban Board")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/tasks-create">
                                            {this.props.t("Create Task")}
                                        </Link>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bxs-user-detail" />
                                    <span>{this.props.t("Contacts")}</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/contacts-grid">{this.props.t("User Grid")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/contacts-list">{this.props.t("User List")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/contacts-profile">
                                            {this.props.t("Profile")}
                                        </Link>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <Link to="/#">
                                    <span className="badge rounded-pill bg-success float-end">
                                        {this.props.t("New")}
                                    </span>
                                    <i className="bx bxs-detail" />

                                    <span>{this.props.t("Blog")}</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/blog-list">{this.props.t("Blog List")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/blog-grid">{this.props.t("Blog Grid")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/blog-details">
                                            {this.props.t("Blog Details")}
                                        </Link>
                                    </li>
                                </ul>
                            </li>

                            <li className="menu-title">Pages</li>
                            <li>
                                <Link to="/#">
                                    <i className="bx bx-user-circle" />
                                    <span className="badge rounded-pill bg-success float-end">
                                        {this.props.t("New")}
                                    </span>
                                    <span>{this.props.t("Authentication")}</span>
                                </Link>
                                <ul className="sub-menu">
                                    <li>
                                        <Link to="/pages-login">{this.props.t("Login")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/pages-login-2">{this.props.t("Login 2")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/pages-register">{this.props.t("Register")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/pages-register-2">
                                            {this.props.t("Register 2")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/page-recoverpw">
                                            {this.props.t("Recover Password")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/pages-recoverpw-2">
                                            {this.props.t("Recover Password 2")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/auth-lock-screen">
                                            {this.props.t("Lock Screen")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/auth-lock-screen-2">
                                            {this.props.t("Lock Screen 2")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/page-confirm-mail">
                                            {this.props.t("Confirm Mail")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/page-confirm-mail-2">
                                            {this.props.t("Confirm Mail 2")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/auth-email-verification">
                                            {this.props.t("Email Verification")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/auth-email-verification-2">
                                            {this.props.t("Email Verification 2")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/auth-two-step-verification">
                                            {this.props.t("Two Step Verification")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/auth-two-step-verification-2">
                                            {this.props.t("Two Step Verification 2")}
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bx-file" />
                                    <span>{this.props.t("Utility")}</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/pages-starter">
                                            {this.props.t("Starter Page")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/pages-maintenance">
                                            {this.props.t("Maintenance")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/pages-comingsoon">
                                            {this.props.t("Coming Soon")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/pages-timeline">{this.props.t("Timeline")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/pages-faqs">{this.props.t("FAQs")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/pages-pricing">{this.props.t("Pricing")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/pages-404">{this.props.t("Error 404")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/pages-500">{this.props.t("Error 500")}</Link>
                                    </li>
                                </ul>
                            </li>

                            <li className="menu-title">{this.props.t("Components")}</li>

                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bx-tone" />
                                    <span>{this.props.t("UI Elements")}</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/ui-alerts">{this.props.t("Alerts")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-buttons">{this.props.t("Buttons")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-cards">{this.props.t("Cards")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-carousel">{this.props.t("Carousel")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-dropdowns">{this.props.t("Dropdowns")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-offcanvas">{this.props.t("OffCanvas")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-grid">{this.props.t("Grid")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-images">{this.props.t("Images")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-lightbox">{this.props.t("Lightbox")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-modals">{this.props.t("Modals")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-rangeslider">
                                            {this.props.t("Range Slider")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-session-timeout">
                                            {this.props.t("Session Timeout")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-progressbars">
                                            {this.props.t("Progress Bars")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-placeholders">{this.props.t("Placeholders")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-sweet-alert">
                                            {this.props.t("Sweet-Alert")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-tabs-accordions">
                                            {this.props.t("Tabs & Accordions")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-typography">
                                            {this.props.t("Typography")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-toasts">{this.props.t("Toasts")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-video">{this.props.t("Video")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-general">{this.props.t("General")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-colors">{this.props.t("Colors")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-rating">{this.props.t("Rating")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-notifications">
                                            {this.props.t("Notifications")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/ui-breadcrumb">
                                            {this.props.t("Breadcrumb")}
                                        </Link>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <Link to="/#">
                                    <i className="bx bxs-eraser" />
                                    <span className="badge rounded-pill bg-danger float-end">
                                        10
                                    </span>
                                    <span>{this.props.t("Forms")}</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/form-elements">
                                            {this.props.t("Form Elements")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/form-layouts">
                                            {this.props.t("Form Layouts")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/form-validation">
                                            {this.props.t("Form Validation")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/form-advanced">
                                            {this.props.t("Form Advanced")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/form-editors">
                                            {this.props.t("Form Editors")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/form-uploads">
                                            {this.props.t("Form File Upload")}{" "}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/form-xeditable">
                                            {this.props.t("Form Xeditable")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/form-repeater">
                                            {this.props.t("Form Repeater")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/form-wizard">{this.props.t("Form Wizard")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/form-mask">{this.props.t("Form Mask")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/dual-listbox">
                                            {this.props.t("Transfer List")}
                                        </Link>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bx-list-ul" />
                                    <span>{this.props.t("Tables")}</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/tables-basic">
                                            {this.props.t("Basic Tables")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/tables-datatable">
                                            {this.props.t("Data Tables")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/tables-responsive">
                                            {this.props.t("Responsive Table")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/tables-editable">
                                            {this.props.t("Editable Table")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/tables-dragndrop">
                                            {this.props.t("Drag & Drop Table")}
                                        </Link>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bxs-bar-chart-alt-2" />
                                    <span>{this.props.t("Charts")}</span>
                                </Link>

                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/apex-charts">{this.props.t("Apex charts")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/chartist-charts">
                                            {this.props.t("Chartist Chart")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/chartjs-charts">
                                            {this.props.t("Chartjs Chart")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/e-charts">{this.props.t("E Chart")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/sparkline-charts">
                                            {this.props.t("Sparkline Chart")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/charts-knob">{this.props.t("Knob Chart")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/re-charts">{this.props.t("Re Chart")}</Link>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bx-aperture" />
                                    <span>{this.props.t("Icons")}</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/icons-boxicons">{this.props.t("Boxicons")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/icons-materialdesign">
                                            {this.props.t("Material Design")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/icons-dripicons">
                                            {this.props.t("Dripicons")}
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/icons-fontawesome">
                                            {this.props.t("Font awesome")}
                                        </Link>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bx-map" />
                                    <span>{this.props.t("Maps")}</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="false">
                                    <li>
                                        <Link to="/maps-google">{this.props.t("Google Maps")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/maps-vector">{this.props.t("Vector Maps")}</Link>
                                    </li>
                                    <li>
                                        <Link to="/maps-leaflet">
                                            {this.props.t("Leaflet Maps")}
                                        </Link>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <Link to="/#" className="has-arrow">
                                    <i className="bx bx-share-alt" />
                                    <span>{this.props.t("Multi Level")}</span>
                                </Link>
                                <ul className="sub-menu" aria-expanded="true">
                                    <li>
                                        <Link to="#">{this.props.t("Level 1.1")}</Link>
                                    </li>
                                    <li>
                                        <Link to="#" className="has-arrow">
                                            {this.props.t("Level 1.2")}
                                        </Link>
                                        <ul className="sub-menu" aria-expanded="true">
                                            <li>
                                                <Link to="#">{this.props.t("Level 2.1")}</Link>
                                            </li>
                                            <li>
                                                <Link to="#">{this.props.t("Level 2.2")}</Link>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </SimpleBar>
            </React.Fragment>
        );
    }
}

SidebarContent.propTypes = {
    location: PropTypes.object,
    t: PropTypes.any,
    type: PropTypes.string,
};

export default withRouter(withTranslation()(SidebarContent));